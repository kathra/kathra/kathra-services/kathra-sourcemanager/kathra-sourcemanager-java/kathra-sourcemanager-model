/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */
package org.kathra.sourcemanager.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
* Folder
*/
@ApiModel(description = "Folder")
public class Folder {

    protected String path = null;


    /**
    * Quick Set path
    * Useful setter to use in builder style (eg. myFolder.path(String).anotherQuickSetter(..))
    * @param String path
    * @return Folder The modified Folder object
    **/
    public Folder path(String path) {
        this.path = path;
        return this;
    }

    /**
    * Folder's path
    * @return String path
    **/
    @ApiModelProperty(value = "Folder's path")
    public String getPath() {
        return path;
    }

    /**
    * Folder's path
    **/
    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Folder folder = (Folder) o;
        return Objects.equals(this.path, folder.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path);
    }
}

